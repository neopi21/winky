
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
//#include <WiFi.h>
#include <InfluxDbClient.h>

#define wifi_ssid "Andrew"
#define wifi_password "G2ElabMonitoringHabitat"

#define mqtt_server "192.168.1.114"
#define mqtt_user "guest"     //s'il a été configuré sur Mosquitto
#define mqtt_password "guest" //idem

// INfluxdb 

// InfluxDB  server url. Don't use localhost, always server name or ip address.
// E.g. http://192.168.1.98:8086 (In InfluxDB 2 UI -> Load Data -> Client Libraries), 
#define INFLUXDB_URL "https://jarvis-oneforall.duckdns.org:8087"
// InfluxDB 2 server or cloud API authentication token (Use: InfluxDB UI -> Load Data -> Tokens -> <select token>)
#define INFLUXDB_TOKEN "AdqkDpwjj_4cN8Iol_EeVabkpOcbYieGJ18D7V-dEqzsb0wOjO1yHPQ6bc91mZ-6AOqgBkbcLp7lStp1NFHABw=="
// InfluxDB 2 organization id (Use: InfluxDB UI -> Settings -> Profile -> <name under tile> )
#define INFLUXDB_ORG "jeedom"
// InfluxDB 2 bucket name (Use: InfluxDB UI -> Load Data -> Buckets)
#define INFLUXDB_BUCKET "OTE"
// InfluxDB v1 database name 
//#define INFLUXDB_DB_NAME "database"

// InfluxDB client instance
//InfluxDBClient clientinflux(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN);
// InfluxDB client instance for InfluxDB 1
//InfluxDBClient client(INFLUXDB_URL, INFLUXDB_DB_NAME);

// Data point
//Point sensor("xKy");

#define RXD2   16             // linky rx  = Serial2 Gpio 16 
#define TXD2   17

#include "linky.h"

//Buffer qui permet de décoder les messages MQTT reçus
char message_buff[100];

long lastMsg = 0;   //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;  //Affiche sur la console si True

uint32_t resetCount = 0;  // keeps track of the number of Deep Sleep tests / resets

/* ======================================================================
  Function: Setup
  Purpose : infinite loop main code
  Input   : -
  Output  : -
  Comments: -
  ====================================================================== */
void setup() { 
  Serial.begin(9600);     //  sortie moniteur 
  delay(500);
  String resetCause = ESP.getResetReason();
  Serial.println(resetCause);
//  resetCount = 0;
  if (resetCause != "Deep-Sleep Wake") {
    ESP.deepSleep(90000000, WAKE_RF_DISABLED); //100ms
//    Serial.println(F("I'm awake and starting the Low Power tests"));
  }
/*
  // Read previous resets (Deep Sleeps) from RTC memory, if any
  uint32_t crcOfData = crc32((uint8_t*) &nv->rtcData.rstCount, sizeof(nv->rtcData.rstCount));
  if ((crcOfData = nv->rtcData.crc32) && (resetCause == "Deep-Sleep Wake")) {
    resetCount = nv->rtcData.rstCount;  // read the previous reset count
    resetCount++;
  }
  nv->rtcData.rstCount = resetCount; // update the reset count & CRC
  updateRTCcrc();
*/
/*  
  Serial.begin(9600);     //  sortie moniteur              
//  delay(500);
  Serial.println(" ");
  Serial.println(F("Boot: version Linky_standart 9600"));
  Serial.print(ESP.getFullVersion()); 
   
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  delay(60000);
  WiFi.forceSleepWake();
  delay(1);
*/



//  delay(1000);
//  cm1106_i2c.read_serial_number();
//  delay(1000);
//  cm1106_i2c.check_sw_version();
//  delay(1000);
  
  linky_setup();
}


//--------------------------------------- LOOP -------------------------------
void loop() {
  
            linky_loop();
}
//-------------------------- fin du programme 
