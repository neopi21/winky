Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

The Code is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

Shield: [![CC BY-NC-ND 4.0][cc-by-nc-nd-shield]][cc-by-nc-nd]

The Hardware is licensed under a
[Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License][cc-by-nc-sa].


[![CC BY-NC-ND 4.0][cc-by-nc-nd-image]][cc-by-nc-nd]

[cc-by-nc-nd]: https://creativecommons.org/licenses/by-nc-nd/4.0/
[cc-by-nc-nd-image]: https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png
[cc-by-nc-nd-shield]: https://img.shields.io/badge/License-CC%20BY--NC--ND%204.0-lightgrey.svg



# WinKy

- WinKy is an Open-source project which creates a Wifi end-device to connect with Linky meter in France.

Software: The most recent and checked PCB is V2: 

History 
- V1 -> First version with manual startup
- V2 -> Autostart when it has stored enough energy 

Hardware: The most recent and checked firmware  is V5:

History
- V1 -> Draft
- V2 -> Work with manual startup
- V3 -> Add deep sleep between transmission
- V4 -> Stop wifi at startup to save enough energy
- V5 -> Use Mac Adress to avoid collision between clients

Explanations are at https://miniprojets.net/index.php/2021/09/08/winky-open-source-projet-pour-linky-avec-wifi/

![Alt text](https://miniprojets.net/wp-content/uploads/2021/09/image-12.png)

- The Lora version of this project can be found at https://miniprojets.net/index.php/2021/07/28/loky-open-source-projet-pour-linky/

This End-device was created by Ferrari Jérôme CNRS/UGA/G-INP – G2ELAB

![CNRS-logo][]
![G2elab-logo][]
![UGA-logo][]
![GINP-logo][]


[GINP-logo]: https://www.grenoble-inp.fr/uas/alias1/LOGO/Grenoble+INP+-+%C3%89tablissement+%28couleur%2C+RVB%2C+120px%29.png
[CNRS-logo]: https://www.cnrs.fr/themes/custom/cnrs/logo.svg
[G2elab-logo]: https://g2elab.grenoble-inp.fr/uas/alias31/LOGO/G2Elab-logo.jpg
[UGA-logo]:https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1D0TxrJin49KlU9Oo263OgjsanU9vKQZhKJ9LZsMjxxggnCEKO7NDsN-rW6CEXGJbLEE&usqp=CAU
