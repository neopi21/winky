Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

# ESP32 Pinout

![ESP32_Dev_board_Pinout](/uploads/d10a23f8db5679d2e06359e37a190fae/ESP32_Dev_board_Pinout.jpg)

# Wireing

|ESP32|---|Winky|
|:-:|:-:|:-:|
|GPIO16|---|RX|
|GND|---|GND|
|3V3|---|3V3|  

![Schéma_de_câblage](/uploads/ef966fc692cdee666ca8fb192db95366/Schéma_de_câblage.png)
![Photo_de_câblage](/uploads/01e1ec4b43796699adf3836538eab7a6/Photo_de_câblage.png)

# Original code

We use the code of [@6galou][1] as base and the historial version of the [Winky][2]

[1] : https://github.com/6galou/TIC-Linky-mode-standart
[2] : https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/winky/-/tree/main/WinKy_Firmware/WinKy_Firmware_V5

# Librairy Add

We add the [Wi-Fi][3] librairy for connection between the Winky and the router

[3] : https://www.upesy.fr/blogs/tutorials/how-to-connect-wifi-acces-point-with-esp32

![CNRS-logo][]
![G2elab-logo][]
![UGA-logo][]
![GINP-logo][]


[GINP-logo]: https://www.grenoble-inp.fr/uas/alias1/LOGO/Grenoble+INP+-+%C3%89tablissement+%28couleur%2C+RVB%2C+120px%29.png
[CNRS-logo]: https://www.cnrs.fr/themes/custom/cnrs/logo.svg
[G2elab-logo]: https://g2elab.grenoble-inp.fr/uas/alias31/LOGO/G2Elab-logo.jpg
[UGA-logo]:https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1D0TxrJin49KlU9Oo263OgjsanU9vKQZhKJ9LZsMjxxggnCEKO7NDsN-rW6CEXGJbLEE&usqp=CAU
