#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define wifi_ssid "YOUR_WIFI_SSID"
#define wifi_password "YOUR_WIFI_PASSWORD"

#define mqtt_server "YOUR_MQTT_IP_ADRESS"
#define mqtt_user "guest"  //s'il a été configuré sur Mosquitto
#define mqtt_password "guest" //idem

#define co2_topic "winky/power"  //Topic co2

char HHPHC;
int ISOUSC;               // intensité souscrite  
int IINST;                // intensité instantanée    en A
int PAPP;                 // puissance apparente      en VA
unsigned long HCHC;       // compteur Heures Creuses  en W
unsigned long HCHP;       // compteur Heures Pleines  en W
unsigned long BASE;       // index BASE               en W
String PTEC;              // période tarif en cours
String ADCO;              // adresse du compteur
String OPTARIF;           // option tarifaire
int IMAX;                 // intensité maxi = 90A
String MOTDETAT;          // status word
boolean teleInfoReceived;

char chksum(char *buff, uint8_t len);
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber);


//CM1106_I2C cm1106_i2c;

//Buffer qui permet de décoder les messages MQTT reçus
char message_buff[100];

long lastMsg = 0;   //Horodatage du dernier message publié sur MQTT
long lastRecu = 0;
bool debug = false;  //Affiche sur la console si True

int ret = 500;
int toto = 550;

// ---------------------------------------------- //
//        Basic constructor for LoKyTIC           //
void TeleInfo() {
  // variables initializations
//  ADCO = "000000000000";
//  OPTARIF = "----";
//  ISOUSC = 0;
//  HCHC = 0L;
//  HCHP = 0L;
//  BASE = 0L;
//  PTEC = "----";
//  HHPHC = '-';
//  IINST = 0;
//  IMAX = 0;
  PAPP = 0;
//  MOTDETAT = "------";
}
// ---------------------------------------------- //

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
//  cm1106_i2c.begin();
  Serial.begin(9600);
  setup_wifi();           //On se connecte au réseau wifi
  client.setServer(mqtt_server, 1883);    //Configuration de la connexion au serveur MQTT
  delay(1000);
//  cm1106_i2c.read_serial_number();
  delay(1000);
//  cm1106_i2c.check_sw_version();
  delay(1000);
}

//Connexion au réseau WiFi
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connexion a ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Connexion WiFi etablie ");
  Serial.print("=> Addresse IP : ");
  Serial.print(WiFi.localIP());
}

//Reconnexion
void reconnect() {
  //Boucle jusqu'à obtenur une reconnexion
  while (!client.connected()) {
    Serial.print("Connexion au serveur MQTT...");
    if (client.connect("BBXCO23", mqtt_user, mqtt_password)) {
      Serial.println("OK");
    } else {
      Serial.print("KO, erreur : ");
      Serial.print(client.state());
      Serial.println(" On attend 5 secondes avant de recommencer");
      delay(5000);
    }
  }
}

// ---------------------------------------------- //
//   Update new values from TIC for the next TX   //
void updateParameters() {
  Serial.begin(1200);  // Important!!! -> RESTART LoKyTIC 
  teleInfoReceived = readTeleInfo();
  Serial.end(); // Important!!! -> STOP LoKyTIC to send packet.
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//           TIC frame capture from Linky         //
boolean readTeleInfo()  {
//boolean readTeleInfo(bool TIC_state)  {
#define startFrame  0x02
#define endFrame    0x03
#define startLine   0x0A
#define endLine     0x0D
#define maxFrameLen 280

  int comptChar=0;  // variable de comptage des caractères reçus 
  char charIn=0;    // variable de mémorisation du caractère courant en réception
  char bufferTeleinfo[21] = "";
  int bufferLen = 0;
  int checkSum;
  int sequenceNumnber = 0;    // number of information group

  //--- wait for starting frame character 
  while (charIn != startFrame)
  { // "Start Text" STX (002 h) is the beginning of the frame
    if (Serial.available())
     charIn = Serial.read()& 0x7F; // Serial.read() vide buffer au fur et à mesure
  } // fin while (tant que) pas caractère 0x02
  //--- wait for the ending frame character 
  while (charIn != endFrame)
  { // tant que des octets sont disponibles en lecture : on lit les caractères
    if (Serial.available()) {
      charIn = Serial.read()& 0x7F;
      // incrémente le compteur de caractère reçus
      comptChar++;
      if (charIn == startLine)  bufferLen = 0;
      bufferTeleinfo[bufferLen] = charIn;
      // on utilise une limite max pour éviter String trop long en cas erreur réception
      // ajoute le caractère reçu au String pour les N premiers caractères
      if (charIn == endLine)  {
        checkSum = bufferTeleinfo[bufferLen -1];
        if (chksum(bufferTeleinfo, bufferLen) == checkSum)  {// we clear the 1st character
          strncpy(&bufferTeleinfo[0], &bufferTeleinfo[1], bufferLen -3);
          bufferTeleinfo[bufferLen -3] = 0x00;
          sequenceNumnber++;
          if (! handleBuffer(bufferTeleinfo, sequenceNumnber))  {
           // Serial.println(F("Sequence error ..."));
            return false;
          }
        }
        else  {
         // Serial.println(F("Checksum error!"));
          return false;
        }
      }
      else 
        bufferLen++;
    }
    if (comptChar > maxFrameLen)  {
     // Serial.println(F("Overflow error ..."));
      return false;
    }
  }
  return true;
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//               TIC frame parsing                //
boolean handleBuffer(char *bufferTeleinfo, int sequenceNumnber) {
  // create a pointer to the first char after the space
  char* resultString = strchr(bufferTeleinfo,' ') + 1;
  boolean sequenceIsOK;

  switch(sequenceNumnber) {
    
  case 1:
    if (sequenceIsOK = bufferTeleinfo[0]=='A')  ADCO = String(resultString);
    break;
  case 2:
    if (sequenceIsOK = bufferTeleinfo[0]=='O')  OPTARIF = String(resultString);
    break;
  case 3:
    if (sequenceIsOK = bufferTeleinfo[1]=='S')  ISOUSC = atol(resultString);
    break;

  case 4:
    if (sequenceIsOK = bufferTeleinfo[3]=='C'){ 
      #define Linky_HCHP true
        #ifdef Linky_HCHP
        HCHC = atol(resultString);
        #endif
    }
    else if (sequenceIsOK = bufferTeleinfo[0]=='B') { 
      #define Linky_BASE true
        #ifdef Linky_BASE
        BASE = atol(resultString);
        #endif

    }
    break;

  case 5:
    if (sequenceIsOK = bufferTeleinfo[3]=='P'){ 
      #define Linky_HCHP true
        #ifdef Linky_HCHP
        HCHP = atol(resultString);
        #endif
    }
    else if (sequenceIsOK = bufferTeleinfo[1]=='T') { 
      #define Linky_BASE true
        #ifdef Linky_BASE
        PTEC = String(resultString);
        #endif
    }
    break;

  case 6:
    if (sequenceIsOK = bufferTeleinfo[1]=='T'){ 
      #define Linky_HCHP true
        #ifdef Linky_HCHP
        PTEC = String(resultString);
        #endif
        }
    else if (sequenceIsOK = bufferTeleinfo[1]=='I') { 
      #define Linky_BASE true
        #ifdef Linky_BASE
        IINST = atol(resultString);
        #endif
        }
    break;
        
  case 7:
    if (sequenceIsOK = bufferTeleinfo[1]=='I'){ 
      #define Linky_HCHP true
        #ifdef Linky_HCHP
        IINST =atol(resultString);
        #endif
        }
    else if (sequenceIsOK = bufferTeleinfo[1]=='M') { 
      #define Linky_BASE true
        #ifdef Linky_BASE
        IMAX =atol(resultString);
        #endif
        }
    break;

  case 8:
    if (sequenceIsOK = bufferTeleinfo[1]=='M') {
      #define Linky_HCHP true
        #ifdef Linky_HCHP
        IMAX =atol(resultString);
        #endif
        }
    else if (sequenceIsOK = bufferTeleinfo[1]=='A') { 
      #define Linky_BASE true
        #ifdef Linky_BASE
        PAPP =atol(resultString);
        #endif
        }
    break;
        
  case 9:
    if (sequenceIsOK = bufferTeleinfo[1]=='A') {
      #define Linky_HCHP true
        #ifdef Linky_HCHP
        PAPP =atol(resultString);
        #endif
        }
    else if (sequenceIsOK = bufferTeleinfo[1]=='H') {
      #define Linky_BASE true
        #ifdef Linky_BASE
        HHPHC = resultString[0];
        #endif
        }
    break;

  case 10:
    if (sequenceIsOK = bufferTeleinfo[1]=='H') { 
      #define Linky_HCHP true
        #ifdef Linky_HCHP
        HHPHC = resultString[0];
        #endif
        }
    else if (sequenceIsOK = bufferTeleinfo[1]=='O') { 
      #define Linky_BASE true
        #ifdef Linky_BASE
        MOTDETAT = String(resultString);
        #endif
        }
    break;
        
  case 11:
    if (sequenceIsOK = bufferTeleinfo[1]=='O')  MOTDETAT = String(resultString);
    break;

  }
    if(!sequenceIsOK) {
     // Serial.print(F("Out of sequence ..."));
    //  Serial.println(bufferTeleinfo);
    }
  return sequenceIsOK;
}
// ---------------------------------------------- //

// ---------------------------------------------- //
//            Checksum Calculation                //
char chksum(char *buff, uint8_t len)  {
  int i;
  char sum = 0;
  for (i=1; i<(len-2); i++) 
    sum = sum + buff[i];
  sum = (sum & 0x3F) + 0x20;
  return(sum);
}
// ---------------------------------------------- //

void loop() {
  TeleInfo();
  
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

   delay(1000);    
    while(PAPP == 0){
      updateParameters();
    }

    client.publish(co2_topic, String(PAPP).c_str(), true);

    delay(1000);

    ESP.deepSleep(60000000);
 
}
